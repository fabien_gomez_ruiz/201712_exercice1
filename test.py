#!/usr/bin/python3.5

import pandas as pd
import unittest
from sources.exercice_1 import Exercice1 as Exercice1
from exercice import Exercice as Exercice

class Test(unittest.TestCase):
    S_FILE = 'sources/signals/signal_S.csv'
    M_FILE = 'sources/signals/signal_M.csv'
    L_FILE = 'sources/signals/signal_L.csv'
    XL_FILE = 'sources/signals/signal_XL.csv'

    KEYS = {
        'S': [[1,1], [3,3]],
        'M': [[3,6], [7,0]],
        'L': [[10,6], [87,98]],
        'XL': [[125,390], [890,703]]
    }

    KEYS_3 = {
        'S': [[1,1], [0,1]],
        'M': [[3,4], [4,2]],
        'L': [[10,6], [49,49]],
        'XL': [[125,390], [445,351]]
    }

    def test_1_read_file(self):
        try:
            signal = Exercice1.read_file(self.S_FILE)

            self.assertEqual(signal[ self.KEYS['S'][0][0] ][ self.KEYS['S'][0][1] ], 0.35)
            self.assertEqual(signal[ self.KEYS['S'][1][0] ][ self.KEYS['S'][1][1] ], 0.09)

            signal = Exercice1.read_file(self.M_FILE)
            self.assertEqual(signal[ self.KEYS['M'][0][0] ][ self.KEYS['M'][0][1] ], 0.78)
            self.assertEqual(signal[ self.KEYS['M'][1][0] ][ self.KEYS['M'][1][1] ], 0.62)

            signal = Exercice1.read_file(self.L_FILE)
            self.assertEqual(signal[ self.KEYS['L'][0][0] ][ self.KEYS['L'][0][1] ], 0.69)
            self.assertEqual(signal[ self.KEYS['L'][1][0] ][ self.KEYS['L'][1][1] ], 0.17)

            signal = Exercice1.read_file(self.XL_FILE)
            self.assertEqual(signal[ self.KEYS['XL'][0][0] ][ self.KEYS['XL'][0][1] ], 0.74)
            self.assertEqual(signal[ self.KEYS['XL'][1][0] ][ self.KEYS['XL'][1][1] ], 0.92)
            
        except Exception:
            self.fail("Une erreur est remontée.")

    def test_2_round(self):
        try:
            signal = Exercice.read_file(self.S_FILE)
            rounded = Exercice1.round(signal)

            self.assertEqual(rounded[ self.KEYS['S'][0][0] ][ self.KEYS['S'][0][1] ], 0)
            self.assertEqual(rounded[ self.KEYS['S'][1][0] ][ self.KEYS['S'][1][1] ], 0)

            signal = Exercice.read_file(self.M_FILE)
            rounded = Exercice1.round(signal)
            self.assertEqual(rounded[ self.KEYS['M'][0][0] ][ self.KEYS['M'][0][1] ], 1)
            self.assertEqual(rounded[ self.KEYS['M'][1][0] ][ self.KEYS['M'][1][1] ], 1)

            signal = Exercice.read_file(self.L_FILE)
            rounded = Exercice1.round(signal)
            self.assertEqual(rounded[ self.KEYS['L'][0][0] ][ self.KEYS['L'][0][1] ], 1)
            self.assertEqual(rounded[ self.KEYS['L'][1][0] ][ self.KEYS['L'][1][1] ], 0)

            signal = Exercice.read_file(self.XL_FILE)
            rounded = Exercice1.round(signal)
            self.assertEqual(rounded[ self.KEYS['XL'][0][0] ][ self.KEYS['XL'][0][1] ], 1)
            self.assertEqual(rounded[ self.KEYS['XL'][1][0] ][ self.KEYS['XL'][1][1] ], 1)
        except Exception:
            self.fail("Une erreur est remontée.")

    def test_3_compress(self):
        try:
            signal = Exercice.read_file(self.S_FILE)
            rounded = Exercice.round(signal)
            compressed = Exercice1.compress(rounded)

            self.assertEqual(compressed[ self.KEYS_3['S'][0][0] ][ self.KEYS_3['S'][0][1] ], 0.5)
            self.assertEqual(compressed[ self.KEYS_3['S'][1][0] ][ self.KEYS_3['S'][1][1] ], 1.0)


            signal = Exercice.read_file(self.M_FILE)
            rounded = Exercice.round(signal)
            compressed = Exercice1.compress(rounded)

            self.assertEqual(compressed[ self.KEYS_3['M'][0][0] ][ self.KEYS_3['M'][0][1] ], 0.5)
            self.assertEqual(compressed[ self.KEYS_3['M'][1][0] ][ self.KEYS_3['M'][1][1] ], 0.75)


            signal = Exercice.read_file(self.L_FILE)
            rounded = Exercice.round(signal)
            compressed = Exercice1.compress(rounded)

            self.assertEqual(compressed[ self.KEYS_3['L'][0][0] ][ self.KEYS_3['L'][0][1] ], 0.5)
            self.assertEqual(compressed[ self.KEYS_3['L'][1][0] ][ self.KEYS_3['L'][1][1] ], 0.75)


            signal = Exercice.read_file(self.XL_FILE)
            rounded = Exercice.round(signal)
            compressed = Exercice1.compress(rounded)

            self.assertEqual(compressed[ self.KEYS_3['XL'][0][0] ][ self.KEYS_3['XL'][0][1] ], 0.75)
            self.assertEqual(compressed[ self.KEYS_3['XL'][1][0] ][ self.KEYS_3['XL'][1][1] ], 1.0)
        except Exception:
            self.fail("Une erreur est remontée.")

if __name__ == '__main__':
    unittest.main()
