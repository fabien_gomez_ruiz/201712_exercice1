import random

def generate_random_file(filename, width, height):
    file = open(filename,”w”) 

    for h in range(height):
        row = []
        for w in range(width):
            row.add(round(random.random(), 2))

        row_str = ','.join(row)

        file.write(row_str)


generate_random_file('test.csv', 3, 3)