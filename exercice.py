import pandas as pd

class Exercice:

    def read_file(filename):
        df = pd.read_csv(filename, header=None)
        return df.values

    def round(values):
        df = pd.DataFrame(values)
        df_rounded = df.round()
        return df_rounded.values.astype(int)

    def compress(values):

        shape = values.shape
        x_points = range(0, shape[0], 2)
        y_points = range(0, shape[1], 2)

        compressed = []

        #values = self.cleaned_signal

        for y in y_points:
            compressed.append([])
            
            for x in x_points:
                compressed_value = (values[y][x] + values[y][x+1] + values[y+1][x] + values[y+1][x+1]) / 4.0
                compressed[int(y/2)].append(compressed_value)
                #print ('%s %s' % (compressed_value, y/2))

        #self.compressed_signal = compressed
        return compressed

