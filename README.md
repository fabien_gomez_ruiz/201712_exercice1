# Exercice 1: Voiture autonome Signal
Dans cet exercice, vous allez développer la partie signal d'une voiture autonome.

Dans l'exercice, vous vous rendrez compte que les 3 challenges sont interdépendants (1- chargement des données, 2 & 3: manipulation des données).
Cependant, lors de l'évaluation, nous analysons chaque challenge séparément. N'hésitez donc pas à remplir chaque partie ;)

##Sensing : Lecture
Vous devez ici lire, à partir d'un fichier, les informations d'un capteur. Lire un fichier et retourner un array de la taille du fichier.
array([line1], [line2])

Ajoutez votre code dans la méthode read_file de l'exercice 1. Ne touchez pas aux paramètres.

read_file(self, path) 
output: array de la taille du fichier (lignes, colonnes)

Voici un exemple de ce qui doit être retourné pour le fichier S:
array([[ 0.02,  0.36,  0.58,  0.63],
       [ 0.69,  0.35,  0.86,  0.6 ],
       [ 0.22,  0.33,  1.  ,  0.09],
       [ 0.82,  0.61,  0.7 ,  0.09]])

Pour cette partie, nous vous encourageons à utiliser read_csv de pandas. Il vous faudra ensuite extraire les valeurs d'un DataFrame pandas vers un array.

##Sensing : signal
La partie sensor de la voiture est chargée de capter des signaux, les nettoyer et les envoyer à la partie suivante.
Cette fonction recevra un array de doubles de 0.0 à 1.0. Vous devrez retourner un array d'integer de 0 à 1 en arrondissant à l'unité la plus proche. 0,5 => 1.Le tableau retourné doit être de la taille de celui en entrée.

##Sensing : compression
A partir d'une image nettoyée, compressez l'image en réalisant la moyenne des 4 cases adjacentes. Le tableau retourné sera deux fois inférieur au tableau entré.

Exemple :
array([[0, 0, 1, 1],
       [1, 0, 1, 1],
       [0, 0, 1, 0],
       [1, 1, 1, 0]])

retournera:
[[0.25, 1.0], [0.5, 0.5]]

Le 0.25 est ainsi la moyenne des positions [0][0], [0][1], [1][0], [1][1].
